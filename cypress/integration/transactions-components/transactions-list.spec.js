/* eslint-disable no-undef */

describe('Transactions list ui', () => {
  
  it('Should return true when title component was correct', () => {
    cy.visit('/');
    cy.contains('Melhor App de Finança').should('to.have.length', 1);
  });

  it('Should add new income', () => {
    cy.visit('/');
    
    cy.get('[type="number"').type('22');
    cy.get('button.income').click();

    cy.get('[type="number"').type('35');
    cy.get('button.income').click();

    cy.get('.list-transactions ul li').should('to.have.length', 2);
    cy.contains('R$ 22,00').should('to.have.length', 1);
    cy.contains('R$ 35,00').should('to.have.length', 1);
  });

  it('Should add new outcome', () => {
    cy.visit('/');
    
    cy.get('[type="number"').type('26.2');
    cy.get('button.outcome').click();

    cy.get('[type="number"').type('52.5');
    cy.get('button.outcome').click();

    cy.get('.list-transactions ul li').should('to.have.length', 2);
    cy.contains('R$ 26,20').should('to.have.length', 1);
    cy.contains('R$ 52,50').should('to.have.length', 1);
  });

  it('Should show the sums of income at entry', () => {
    cy.visit('/');
    
    cy.get('[type="number"').type('26.2');
    cy.get('button.income').click();

    cy.get('[type="number"').type('52.5');
    cy.get('button.income').click();

    cy.get('.score .income p').contains('R$ 78,70');
  });

  it('Should show the sums of outcome at output', () => {
    cy.visit('/');
    
    cy.get('[type="number"').type('23.7');
    cy.get('button.outcome').click();

    cy.get('[type="number"').type('66.3');
    cy.get('button.outcome').click();

    cy.get('.score .outcome p').contains('R$ 90,00');
  });

  it('Should show the sums of total of income minus outcome', () => {
    cy.visit('/');

    cy.get('[type="number"').type('1376.78');
    cy.get('button.income').click();

    cy.get('[type="number"').type('234.59');
    cy.get('button.outcome').click();

    cy.get('.score .total p').contains('R$ 1.142,19');
  });


});